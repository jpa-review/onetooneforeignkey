package com.twuc.webApp.entities;

import com.twuc.webApp.repositories.HusbandRepository;
import com.twuc.webApp.repositories.WifeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OneToOneForeignKeyTest {
    @Autowired
    HusbandRepository husbandRepository;

    @Autowired
    WifeRepository wifeRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_by_one_to_one_foreign_key() {
        Husband husband = new Husband("李嘉豪");
        Wife wife = new Wife("桥本环奈");

        husband.setWife(wife);

        husbandRepository.saveAndFlush(husband);
        entityManager.clear();

        Husband husbandResult = husbandRepository.findById(1L).orElseThrow(RuntimeException::new);
        Wife wifeResult = wifeRepository.findById(1L).orElseThrow(RuntimeException::new);

        assertEquals("李嘉豪", husbandResult.getName());
        assertEquals("桥本环奈", wifeResult.getName());
    }
}