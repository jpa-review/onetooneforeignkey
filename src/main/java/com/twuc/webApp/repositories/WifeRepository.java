package com.twuc.webApp.repositories;

import com.twuc.webApp.entities.Wife;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WifeRepository extends JpaRepository<Wife, Long> {
}
