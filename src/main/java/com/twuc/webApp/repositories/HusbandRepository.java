package com.twuc.webApp.repositories;

import com.twuc.webApp.entities.Husband;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HusbandRepository extends JpaRepository<Husband, Long> {
}
